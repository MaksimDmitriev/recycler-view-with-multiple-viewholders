package ru.maksim.kotlin.recycler

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private const val TAG = "RecyclerViewStub"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView = findViewById<RecyclerView>(R.id.rec_view)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val array = Array(size = 100) {
            Item("item $it", it % 2)
        }

        recyclerView.adapter = CustomAdapter(array)
    }
}

class CustomAdapter(private val dataSet: Array<Item>) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    open class ViewHolder(view: View, val titleView: TextView) :
        RecyclerView.ViewHolder(view)

    class BasicViewHolder(view: View, titleView: TextView) : ViewHolder(view, titleView) {

        val extraTextView: TextView = view.findViewById(R.id.text_view_basic)

    }

    class BasicListViewHolder(
        view: View,
        titleView: TextView
    ) : ViewHolder(view, titleView) {

        val extraTextView1: TextView = view.findViewById(R.id.list_item_1)
        val extraTextView2: TextView = view.findViewById(R.id.list_item_2)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.d(TAG, "onCreateViewHolder")

        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_view, parent, false)
        val extraLayout = when (viewType) {
            Item.TYPE_BASIC -> R.layout.content_basic
            Item.TYPE_BASIC_LIST -> R.layout.content_basic_list
            else -> 0
        }
        val stub = view.findViewById<ViewStub>(R.id.extra_content)
        stub.layoutResource = extraLayout
        stub.inflate()

        val titleView = view.findViewById<TextView>(R.id.title_view)
        return when (viewType) {
            Item.TYPE_BASIC -> BasicViewHolder(view, titleView)
            Item.TYPE_BASIC_LIST -> BasicListViewHolder(view, titleView)
            else -> throw IllegalStateException("not implemented")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d(TAG, "onBindViewHolder for pos $position")
        holder.titleView.text = dataSet[position].title
        when (dataSet[position].type) {
            Item.TYPE_BASIC -> setBasicContent(holder, position)
            Item.TYPE_BASIC_LIST -> setBasicListContent(holder, position)
        }
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        Log.d(TAG, "onViewAttachedToWindow for pos ${holder.adapterPosition}")
    }

    private fun setBasicListContent(holder: ViewHolder, position: Int) {
        val viewHolder = holder as BasicListViewHolder
        viewHolder.extraTextView1.text = "first extra text at pos $position"
        viewHolder.extraTextView2.text = "second extra text at pos $position"
    }

    private fun setBasicContent(holder: ViewHolder, position: Int) {
        val viewHolder = holder as BasicViewHolder
        viewHolder.extraTextView.text = "extra text at pos $position"
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun getItemViewType(position: Int): Int {
        return dataSet[position].type
    }

}

data class Item(val title: String, val type: Int) {

    companion object {
        const val TYPE_BASIC = 0
        const val TYPE_BASIC_LIST = 1
    }
}